//DO THESE ON DOCUMENT READY FOR ALL PAGES
$(document).ready(function(){
	addHeaderFooter('header','footer');
});

//DO THESE ON WINDOW LOAD FOR PAGES WHERE TARGET ELEMENT IS PRESENT
$(window).load(function(){
	if($('.exit').length){
		$("#exitinfo").load("../views/html_inc/exitinfo.html");
	}
	if($('#locate').length){
		$("#locateinfo").load("../views/html_inc/locateinfo.html");
	}
});

//DO THESE ON WINDOW LOAD FOR ALL PAGES
$(window).load(function(){
	//$("#privacypolicy").load("../views/html_inc/privacypolicy.html");
	//$("#tsandcs").load("../views/html_inc/tsandcs.html");
	//$("#contactinfo").load("../views/html_inc/contactinfo.html");
	$.fn.slideFadeToggle = function(speed, easing, callback) {
	    return this.animate({
	        opacity: 'toggle',
	        height: 'toggle'
	    }, speed, easing, callback);
	};
	
	$('body').delegate('input[name*="account-type-select"]', 'click', function(){
	//$('input[name*="account-type-select"]').click(function(){
		var stepid = $(this).attr('stepid');
		if($(this).val() == "yes"){ 
			$('#' + stepid + 'fname, #' + stepid + 'fname input, #' + stepid + 'lname, #' + stepid + 'lname input').removeClass('span5').addClass('span3');
			$('#' + stepid + 'bizname').show();
		}
		if($(this).val() == "no"){ 
			$('#' + stepid + 'fname, #' + stepid + 'fname input, #' + stepid + 'lname, #' + stepid + 'lname input').removeClass('span3').addClass('span5');
			$('#' + stepid + 'bizname').hide();
		}
	});
	
	localStorageInit();
	localStorageGo();
});

//DO THESE ON DOCUMENT READY FOR ALL PAGES WHERE TARGET ELEMENT IS PRESENT
$(document).ready(function(){
	if($('.claimstep').length){
		claimSteps();
	}
});

//**********  Asurion jQuery Functions  **********//
//**********  Created By: Bryan Haddock **********//
//**********  Last Updated: 07-09-2013  **********//

/**
*** CONTENTS ***
* General Template Functions:
	- headerSteps - indicates current step based on hidden form input#step value
* Form Field Functions:
	- dropDownSelect - adds dropdownselect fields
	- requiredInputValidation - validates .required fields
	- requiredMatchValidation - validates matching of .required-match fields
	- activateSubmit - activates submit button when .required and .required-match fields are .verified
*/

/**
* Print This
* @param {Selector} triggerelement
* @param {Selector} elementtoprint
*/

function printPopUp(triggerelement,elementtoprint) {
	$(triggerelement).click(function(){
		var data = $(elementtoprint).html();
		var mywindow = window.open('', elementtoprint, 'height=400,width=600');
		mywindow.document.write('<html><head><title>print</title>');
		mywindow.document.write('<link rel="stylesheet" href="../styles/base/bootstrap.css" type="text/css" />');
		mywindow.document.write('<link rel="stylesheet" href="../styles/base/bootstrap-responsive.css" type="text/css" />');
		mywindow.document.write('<link rel="stylesheet" href="../styles/base/style.css" type="text/css" />');
		mywindow.document.write('</head><body >');
		mywindow.document.write(data);
		mywindow.document.write('</body></html>');
		
		mywindow.print();
		mywindow.close();
		
		return true;		
	});
}

/**
* Local Storage Init
*/

function localStorageInit(){
    $('input,textarea').each(function(){
    	var thisname = $(this).attr('name');
    	var thisid = $(this).attr('id');
    	
    	if((thisname != undefined) && (thisid != undefined) && ($(this).hasClass('stored'))){
		    if (localStorage[thisname]) {
		        $('#' + thisid).val(localStorage[thisname]);
		    }
    	}
    });
}

/**
* Local Storage Actions
*/

function localStorageGo(){
	$('.stored').keyup(function () {
	    localStorage[$(this).attr('name')] = $(this).val();
	});	

	$('#localStorageTest').submit(function() {
	    localStorage.clear();
	});
}

/**
* Checkbox Maker
*/

function cheekyBox() {
	document.createElement('cheekybox'); //crutch for IE
	$('cheekybox').each(function(){
		var cheekyid = $(this).attr('id');
		var nameid = 'name="' + cheekyid + '" id="' + cheekyid + '"';
		var divclass = $(this).attr('class');
		var size = ($(this).attr('size')=="large")? 'lg' : 'sm';
		var label = $(this).attr('label');
		var target = ($(this).attr('targetid') != undefined)? ' targetid="' + $(this).attr('targetid') + '"' : '';
		var checkboxinput = ($(this).attr('createinput')=="yes")? '<input type="hidden" ' + nameid + ' value="off" />' : '';
		
		var output = 
			'<div class="' + divclass + '">' + checkboxinput +
			'<div ' + nameid + ' class="cheekybox cheekybox-' + size + '" ' + target + ' ></div>' +
			'<label for="' + cheekyid + '" class="cheekybox-' + size + '-label">' + label + '</label>' +
			'</div>';
			
		$(this).replaceWith(output);
	});
	$('body').delegate('.cheekybox', 'click', function(){
		var cheekyid = $(this).attr('id');
		var targetid = $(this).attr('targetid');
		if($(this).hasClass('icon-checkmark')){
			$(this).removeClass('icon-checkmark');
			if(targetid != undefined){ $('#' + targetid + '.btn').removeClass('btn-primary').addClass('btn-disabled'); }
			$('input#' + cheekyid).val("off");
		}else{
			$(this).addClass('icon-checkmark');
			if(targetid != undefined){ $('#' + targetid + '.btn').removeClass('btn-disabled').addClass('btn-primary'); }
			$('input#' + cheekyid).val("on");
		}
	});		
}

/**
* add Feader & Footer
*/

function addHeaderFooter(headerfile, footerfile) {
	$('body').prepend("<div id='header'></div>");
	$('#header').load("../views/html_inc/" + headerfile + ".html");
	$('body').wrapInner("<div class='pre-footer'></div>");
	var windowheight = $('body').height();
	$('.pre-footer').append('<div class="push"></div>');
	$('body').append("<div id='footer'></div>");
	$('#footer').load("../views/html_inc/" + footerfile + ".html");
}

/**
* claimSteps
*/

function claimSteps() {
	$('.claimstep').each(function(){
		stepnum = $(this).attr('stepnum');
		title = $(this).attr('title');
		iconclass = $(this).attr('iconclass');
		if(stepnum == 1){ var stepnum_text = "one"; }
		if(stepnum == 2){ var stepnum_text = "two"; }
		if(stepnum == 3){ var stepnum_text = "three"; }
		if(stepnum == 4){ var stepnum_text = "four"; }
		if(stepnum == 5){ var stepnum_text = "five"; }
		if(stepnum == 6){ var stepnum_text = "six"; }
		if(stepnum == 7){ var stepnum_text = "seven"; }
		if(stepnum == 8){ var stepnum_text = "eight"; }
		
		var titlebar = 
			'<div class="row"><div id="step' + stepnum + '" class="span12">' +
				'<div class="stepbar inactive">' +
					'<i class="' + iconclass + '"></i>' +
					'<div class="stepbar-text-top">step ' + stepnum_text + '</div>' +
					'<div class="stepbar-text-bottom">' + title + '</div>' +
				'</div>' +
			'</div></div>\n';
		
		$(this).before(titlebar);
		$(this).load('../views/steps_inc/step' + stepnum + '.html');
		$(this).hide();
	});
	
	$(document).ready(function(){
		$('.claimstep[stepnum="1"]').css('display','block');
		$('#step1 .stepbar').removeClass('inactive');
	});
	
	$(window).load(function(){
		$('body').delegate('.step-continue', 'click', function(){
			cheekyBox();
			//alert('stupid cheekybox');
			var tostep = $(this).attr('tostep');
			var thisstep = $(this).parents('.claimstep').attr('stepnum');
			var nextstep = (tostep == undefined)? parseInt(thisstep,10) + 1 : tostep;

			//alert('FART DONKEY!');
			$(this).parents('.claimstep').animate({ height: 'toggle', opacity: 'toggle' }, 'slow');
			$('#step' + thisstep + ' .stepbar').addClass('complete');
			$('#step' + thisstep + ' .stepbar i').removeClass().addClass('icon-checkmark');
			$('.claimstep[stepnum="' + nextstep + '"]').css('display','block');

			$('#step' + nextstep + ' .stepbar').removeClass('inactive');
			$('html, body').animate({
			scrollTop: $('#step' + thisstep + ' .stepbar').offset().top + 64}, 1000);			
		});
	});
}

/**
* dropDownSelect
*/

function dropDownSelect() {
	document.createElement('dropdownselect'); //crutch for IE
	$('dropdownselect').each(function(){
		var id = $(this).attr('id');
		var label = $(this).attr('label');
		var readonly = $(this).attr('readonly')		
		var inputclass = $(this).attr('inputclass');
		var tabindex = $(this).attr('tabindex');
		var inputname = $(this).attr('inputname');
		var defaultval = $(this).attr('defaultval');
		var placeholder = $(this).attr('placeholder');
		var iconclass = $(this).attr('iconclass');
		var listvals = $(this).attr('listvals');
		var listarray = listvals.split(',');
		
		var labeloutput = (label == "") ? '' : '<label for="' + id + '">' + label + '</label>\n';
		var labellistclass = (label == "") ? 'no-label' : 'has-label';

		var listoutput = '<ul class="ddslist ' + inputclass + '">';
		$.each(listarray, function() {
			var elements = this.split(':');
			var name = elements[0];
			var thumb = elements[1];
			
			var liclass = (thumb == null) ? 'class="no-thumb"' : 'class="has-thumb" style="background-image: url(' + thumb + ')"';
			
			listoutput += '<li input="' + inputname + '" ' + liclass + ' >' + name + '</li>';
		});
		listoutput += '</ul>';
		
		var output =
			labeloutput +
			'<div class="input-append dropdownselect ' + inputclass + '">\n' +
			'<input id="' + id + '" readonly="' + readonly + '"  placeholder="' + placeholder + '" class="' + inputclass + ' ddsinput" name="' + inputname + '" type="text" value="' + defaultval + '" tabindex="' + tabindex + '">\n' +
			'<span class="add-on"><i class="' + iconclass + '"></i></span>\n' +
			'</div>\n' +
			'<div class="dropdownselect-list ' + inputclass + ' ' + labellistclass + '">\n' +
			listoutput +
			'</div>\n';
					
		$(this).replaceWith(output);

		var inputheight = $('input#' + id).outerHeight();
		$('input#' + id).next('.add-on').css('height',inputheight-2);
		$('input#' + id).next('.add-on').children('i').css('line-height',inputheight-4 + 'px');
		
		if($('.ddslist li').hasClass('has-thumb')){
			$('.ddslist').addClass('has-thumb');
		}
	});
	
	$('.dropdownselect').click(function() {
		$(this).next('.err').remove();
		var inputwidth = $(this).find('.ddsinput').outerWidth();
		var inputname = $(this).find('.ddsinput').attr('name');
		$('li[input="' + inputname +'"].no-thumb').parent().css('width', inputwidth );
		$('li[input="' + inputname +'"].has-thumb').parent().css('width', inputwidth );
		$(this).next('.dropdownselect-list').show();
	});

	$('.dropdownselect-list li').click(function() {
		var selected = $(this).text();
		var targetinput = $(this).attr('input');
		$('input[name="' + targetinput + '"]').val(selected);
		$(this).parents('.dropdownselect-list').hide();
	});

	$(document).mouseup(function(e) {
	    var container = $('.dropdownselect').next('.dropdownselect-list');
	    if (container.has(e.target).length === 0){
	        container.hide();
	    }
	});	
}

/* FORM RELATED JS */

/**
* requiredInputValidation
*/

function requiredInputValidation() {
	$('.required').blur(function() {
		var inputvalue = $(this).val();
		var errmsg = $(this).attr('errmsg');
		if( inputvalue == $(this)[0].defaultValue && inputvalue.length == 0 ){
			$(this).addClass('not-verified');
			$(this).before('<span class="icon-close textbox-icon"></span>');
			$(this).after('<div class="err">' + errmsg + '</div>');
		}else{
			$(this).addClass('verified');
			$(this).before('<span class="icon-checkmark textbox-icon"></span>');
		}
	});
}

/**
* requiredMatchValidation
* adds .not-verified or .verified to input elements using .required-match
* Indicate selector for matching by setting "matchwith" attribute to the selector to match $(this).val() with. "errmsg" attribute indicates the error message that is displayed after the input when the values do not match.
*/

function requiredMatchValidation() {
	$('.required-match').focus(function() {
		$(this).removeClass('not-verified').removeClass('verified');
		$(this).prev('.textbox-icon').remove();
		$(this).next('.err').remove();
		$(this).val('');
		$('.err.submit').remove();
	});
	$('.required-match').blur(function() {
		$(this).each(function() {
			var inputvalue = $(this).val();
			var errmsg = $(this).attr('errmsg');
			var matchwith = $(this).attr('matchwith');
			var comparevalue = $(matchwith).val();
			if( inputvalue != comparevalue || ( inputvalue == $(this)[0].defaultValue && inputvalue.length == 0 )){
				$(this).addClass('not-verified');
				$(this).before('<span class="icon-close textbox-icon"></span>');
				$(this).after('<div class="err">' + errmsg + '</div>');
			}else{
				$(this).addClass('verified');
				$(this).before('<span class="icon-checkmark textbox-icon"></span>');				
			}
		});
	});
}

/**
* activateSubmit
* @param {Selector} submitselector
*/

function activateSubmit(submitselector) {
	$('.required, .required-match').focus(function() {
		$(this).removeClass('not-verified').removeClass('verified');
		$(this).prev('.textbox-icon').remove();
		$(this).next('.err').remove();
		$(this).val('');
		$('.err.submit').remove();
		if($(submitselector).hasClass('active')){
			$(submitselector).removeClass('active').addClass('disabled');
		}
	});
	$('.required,.required-match').blur(function() {
		var nreq = $('input.required, input.required-match').length;
		var nver = $('input.verified, input.required-match.verified').length;
		if (nreq == nver){
			$(submitselector).removeClass('disabled').addClass('active');
		}
	});
}



